import { combineReducers } from 'redux';
import collections from './collectionReducer';

export const replaceOrAddCollectionElement = (collection, element, idAttr) => {
  const el = collection.find((search) => search.id === idAttr);
  // if existing remove it from the list
  if (el) collection.splice(collection.indexOf(el), 1);
  // add element at the beginning of the list
  collection.unshift({ id: idAttr, value: element});
  return [ ...collection ];
}

export default combineReducers({
  collections
});