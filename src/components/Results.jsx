import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// actions
import { clearResults } from '../actions/userActions';
// selectors
import { selectUsers } from '../selectors/userSelectors';

const Body = styled('div')`
	text-align: center;
	background-color: #F7F7F7;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	width: 50%;

	@media all and (max-width: 1000px)  {
		width: 100%;
		height: 100%;
  	}
`;

const Line = styled('div')`
	background-color: white;
	color: ${props => props.known ? 'green' : 'red'};
	width: 80%;
	margin: 0 auto 10px auto;
`;

const Button = styled('button')`
	background-color: #802254;
	color: white;
	border: 0px;
	margin: auto;
	margin-bottom: 20px;
	height: 60px;
	width: 100px;
	border-radius: 5px;

	:focus {
		outline-width: 0;
	}
`;

const Results = ({ clear, list }) => (
	<Body>
		<h3>Results</h3>
		{list.map((user) => (
			<Line key={user.id} known={user.value} >
				<p>
					{!user.value && (
						<span>BOOH we don't know </span>
					)}
					{user.id}
					{user.value && (
						<span> is a GDC member</span>
					)}
				</p>
			</Line>
		))}
		{!!list.length && (
			<Button	onClick={() => clear()}>
				Clear
			</Button>
		)}
	</Body>
);

Results.propTypes = {
	clear: PropTypes.func.isRequired,
	list: PropTypes.array.isRequired
};

const mapState = (state) => ({
    list: selectUsers(state)
});

const mapDispatch = (dispatch) => ({
    clear: () => dispatch(clearResults())
});

export default connect(mapState, mapDispatch)(Results);
