import { USERS } from '../constants/collectionTypes';

export const selectUsers = (state) => state.collections[USERS];
