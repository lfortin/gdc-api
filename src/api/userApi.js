import { get } from '../utilities/apiUtilities';

export const fetchUser = (email) => get({
	url: `/?email=${email}`
})