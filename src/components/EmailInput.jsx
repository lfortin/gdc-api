import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// actions
import { searchUser } from '../actions/userActions';

const Body = styled('div')`
	display: flex;
	flex-direction: row;
	justify-content: center;
	padding: 10px;
	max-width: 100%;

	@media all and (max-width: 1000px)  {
		flex-direction: column;

		div, button {
			margin: auto
		}

		button {
			width: 350px;
			max-width: 100%;
			margin-top: 10px;
		}
		div {
			width: 340px;
		}
  	}
`;

const Input = styled('div')`
	border: 1px solid pink;
	border-radius: 5px;
	height: 50px;
	width: 350px;
	max-width: 100%;
	display: flex;
	justify-content: space-between;
	padding: 5px;

	input {
		border: 0px;
		:focus {
			outline-width: 0;
		}
	}

	span {
		margin: auto 0;
	}
`;

const Button = styled('button')`
	background-color: #802254;
	color: white;
	border: 0px;
	margin-left: 20px;
	height: 60px;
	width: 100px;
	border-radius: 5px;

	:focus {
		outline-width: 0;
	}

	:disabled {
		background-color: #B37B99;
	}
`;

class EmailInput extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: ''
		}
	}

	render = () => {
		const { emailDomain, search } = this.props;
		const { email } = this.state;
		return (
			<Body>
				<Input>
					<input
						type="text"
						placeholder="john.doe"
						value={email}
						onChange={(e) => this.setState({
							email: e.target.value
						})}	
					/>
					<span>{`@${emailDomain}`}</span>
				</Input>
				<Button
					disabled={!email}
					onClick={() => search(`${email}@${emailDomain}`)}
				>
					Submit
				</Button>
			</Body>
		)
	}
};

EmailInput.propTypes = {
	emailDomain: PropTypes.string.isRequired,
	search: PropTypes.func.isRequired
};

const mapDispatch = (dispatch) => ({
	search: (email) => dispatch(searchUser(email))
});

export default connect(null, mapDispatch)(EmailInput);
