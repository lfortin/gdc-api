import React from 'react';
import styled from 'styled-components';
// components
import Header from './Header';
import Search from './Search';
import Results from './Results';

const App = styled('div')`
	text-align: center;
	display: flex;
	flex-flow: column;
	height: 100vh;
	width: 100%;
`;

const Body = styled('div')`
	flex-grow : 1;
	display: flex;

	h3 {
		margin: 50px auto;
	}

	@media all and (max-width: 1000px)  {
		flex-flow: row;
		flex-direction: column;
  	}
`;

const Home = () => (
	<App>
		<Header />
		<Body>
			<Search />
			<Results />
		</Body>
	</App>
);

Home.propTypes = {};

export default Home;
