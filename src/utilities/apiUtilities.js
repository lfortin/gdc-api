import axios from 'axios';

const api = axios.create({
	baseURL: 'http://localhost:5000'
});

const getHeaders = (config) => ({
  ...config.headers,
  'Accept': 'Application/json'
});

export const get = (config) => api({
  method: 'GET',
  ...config,
  headers: getHeaders(config)
});

export default { get };
