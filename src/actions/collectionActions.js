import { RESET_COLLECTION, SET_COLLECTION_ELEMENT } from '../actionTypes';

export const setUserCollectionElement = (name, value, idAttr) => ({
  type: SET_COLLECTION_ELEMENT,
  payload: {
    name,
    value,
    idAttr
  }
});

export const resetUserCollectionElement = (name) => ({
  type: RESET_COLLECTION,
  payload: { name }
});