import React, { Component } from 'react';
import styled from 'styled-components';
// Components
import EmailInput from './EmailInput';

const Body = styled('div')`
	text-align: center;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	width: 50%;

	@media all and (max-width: 1000px)  {
		width: 100%;
		margin-bottom: 20px;
  	}
`;

const Separator = styled('span')`
	text-transform: UpperCase;
	margin: 10px 0;

	&:before, &:after {
		content:'';
		display:inline-block;
		vertical-align: middle;
		width: 35%;
		height: 1px;
		background: #000
	}
	&:before {
		margin-right: .3rem
	}
	&:after {
		margin-left: .3rem
	}
`;

class Search extends Component {
	render = () => {
		return (
			<Body>
				<h3>Enter your email address</h3>
				<EmailInput emailDomain="gmail.com" />
				<Separator>or</Separator>
				<EmailInput emailDomain="gensdeconfiance.com" />
			</Body>
		)
	}
};

Search.propTypes = {};

export default Search;
