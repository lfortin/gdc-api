import React from 'react';
import styled from 'styled-components';

const Head = styled('div')`
    display: flex;
    justify-content: space-between;
    padding: 15px;
    height: 50px;
    border-bottom: 2px solid pink;
`;

const Header = () => (
    <Head>
        <p>Louis Fortin</p>
        <p>05/08/2020</p>
    </Head>
);

Header.propTypes = {};

export default Header;
