import { RESET_COLLECTION, SET_COLLECTION_ELEMENT } from '../actionTypes';
import { USERS } from '../constants/collectionTypes';
import { replaceOrAddCollectionElement } from './index';

export const initialCollectionState = [];

const initialState = {
  [USERS]: [ ...initialCollectionState ],
};

/**
 * This reducer keeps references to certain collections of data
 */
const collectionReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_COLLECTION_ELEMENT: {
      const { name, value, idAttr } = action.payload;
      return {
        ...state,
        [name]: replaceOrAddCollectionElement(state[name], value, idAttr)
      };
    }
    case RESET_COLLECTION: {
      const { name } = action.payload;
      return {
        ...state,
        [name]: [ ...initialCollectionState ]
      };
    }
    default: {
      return state;
    }
  }
};

export default collectionReducer;
