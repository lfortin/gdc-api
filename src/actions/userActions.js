import {
  resetUserCollectionElement,
  setUserCollectionElement
} from './collectionActions';
import { USERS } from '../constants/collectionTypes';
import { fetchUser } from '../api/userApi';

export const searchUser = (email) => dispatch => fetchUser(email)
  .then(({ data: { exists }}) =>  dispatch(setUserCollectionElement(USERS, exists, email)));

export const clearResults = () => dispatch => dispatch(
  resetUserCollectionElement(USERS)
)
